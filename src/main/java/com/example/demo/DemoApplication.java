package com.example.demo;


import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;



@SpringBootApplication
public class DemoApplication {

    private static final Logger logger = LoggerFactory.getLogger(DemoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    /** Бин добавляет в базу 6 пользователей и добавляет их друг другу в друзья*/
    @Bean
    public CommandLineRunner setup(UserRepository userRepository) {

        User user1 = new User("TEST1_NAME","TEST_1_SECOND_NAME","TEST_1_SURNAME", "2005-08-12");
        User user2 = new User("TEST2_NAME","TEST_2_SECOND_NAME","TEST_2_SURNAME", "2005-08-12");
        User user3 = new User("TEST3_NAME","TEST_3_SECOND_NAME","TEST_3_SURNAME", "2005-08-12");
        User user4 = new User("TEST4_NAME","TEST_4_SECOND_NAME","TEST_4_SURNAME", "2008-11-02");
        User user5 = new User("TEST5_NAME","TEST_5_SECOND_NAME","TEST_5_SURNAME", "2012-01-11");
        User user6 = new User("TEST6_NAME","TEST_6_SECOND_NAME","TEST_6_SURNAME", "2007-01-13");

        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);
        userRepository.save(user4);
        userRepository.save(user5);
        userRepository.save(user6);

        user1.addFriend(user5);
        user5.addFriend(user1);
        user5.addFriend(user3);
        user3.addFriend(user5);
        user3.addFriend(user4);
        user4.addFriend(user3);
        user4.addFriend(user2);
        user2.addFriend(user4);

        return (args) -> {
            userRepository.save(user1);
            userRepository.save(user2);
            userRepository.save(user3);
            userRepository.save(user4);
            userRepository.save(user5);
            userRepository.save(user6);


        };
    }
}