package com.example.demo.service;

import java.util.List;

import com.example.demo.exception.InvalidDataException;
import com.example.demo.model.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    User addUser(User user) throws InvalidDataException;
    ResponseEntity<?> delete(long id);
    User getById(Long id);
    List<User> getByName(String name);
    List<User> getBySurname(String surname);
    User updateUser(User user, Long id) throws InvalidDataException;
    List<User> getAll();
    List<User> getAfter(String birthday) throws InvalidDataException;
    List<User> getBefore(String birthday) throws InvalidDataException;
    User addFriend(Long id, Long friendId) throws InvalidDataException;
    User deleteFriend(Long id, Long friendId) throws InvalidDataException;
    List<User> getChain(Long id1, Long id2) throws InvalidDataException;


}