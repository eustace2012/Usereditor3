package com.example.demo.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.example.demo.exception.InvalidDataException;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service

/**
 * Класс сервис, делает запросы к UserRepository
 */
public class UserServiceImpl implements UserService {


    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public User addUser(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public ResponseEntity<?> delete(long id) {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", id));

        userRepository.delete(user);
        return ResponseEntity.ok().build();

    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", id));
    }

    @Override
    public List<User> getByName(String name) {
        List<User> user =  userRepository.findByName(name);
        if(user.size() == 0) {
            throw new ResourceNotFoundException("User", "name", name);
        } else {
            return user;
        }
    }


    @Override
    public List<User> getBySurname(String surname) {

        List<User> user = userRepository.findBySurname(surname);
        if(user.size() == 0) {
            throw new ResourceNotFoundException("User", "surname", surname);
        } else {
            return user;
        }
    }

    @Override
    @Transactional
    public User updateUser(User userDetails, Long id) throws InvalidDataException {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", id));

        if(userDetails.getName().length()>255){
            throw new InvalidDataException(" имя не должно превышать 255 символов");
        }
        if(userDetails.getSecondName().length()>255){
            throw new InvalidDataException("отчество не должно превышать 255 символов");
        }
        if(userDetails.getSurname().length()>255){
            throw new InvalidDataException("фамилия не должна превышать 255 символов");
        }

        user.setName(userDetails.getName());
        user.setSecondName(userDetails.getSecondName());
        user.setSurname(userDetails.getSurname());
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(userDetails.getBirthday());
        } catch (Exception ex) {

            throw new InvalidDataException("дата рождения должна быть в формате yyyy-MM-dd");

        }

        user.setBirthday(userDetails.getBirthday());

        User updatedUser = userRepository.save(user);
        return updatedUser;

    }

    @Override
    public List<User> getAll() {

        return userRepository.findAll();
    }

    @Override
    public List<User> getAfter(String birthday) throws InvalidDataException {

        Date d = null;
        try {
            d = new SimpleDateFormat("yyyy-MM-dd").parse(birthday);
        } catch (Exception ex) {
            throw new InvalidDataException("дата  должна быть в формате yyyy-MM-dd");
        }

        List<User> user =  userRepository.dateLater(birthday);
        if(user == null) {
            throw new ResourceNotFoundException("User", "birthday", ">"+birthday);
        } else {
            return user;
        }
    }

    @Override
    public List<User> getBefore(String birthday) throws InvalidDataException {
        Date d = null;
        try {
            d = new SimpleDateFormat("yyyy-MM-dd").parse(birthday);
        } catch (Exception ex) {
            throw new InvalidDataException("дата  должна быть в формате yyyy-MM-dd");
        }

        List<User> user =  userRepository.dateEarlier(birthday);
        if(user == null) {
            throw new ResourceNotFoundException("User", "birthday", "<"+birthday);
        } else {
            return user;
        }
    }

    @Override
    @Transactional
    public User addFriend(Long id, Long friendId) throws InvalidDataException {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", id));

        User friend = userRepository.findById(friendId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", friendId));

        user.addFriend(friend);
        friend.addFriend(user);

        User updatedUser = userRepository.save(user);
        userRepository.save(friend);
        return updatedUser;
    }

    @Override
    @Transactional
    public User deleteFriend(Long id, Long friendId) throws InvalidDataException {

        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", id));

        User friend = userRepository.findById(friendId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", friendId));

        user.deleteFriend(friend);
        friend.deleteFriend(user);
        User updatedUser = userRepository.save(user);
        userRepository.save(friend);
        return updatedUser;
    }

    @Override
    public List<User> getChain(Long id1, Long id2) throws InvalidDataException {

        User user = userRepository.findById(id1)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", id2));
        User friend = userRepository.findById(id2)
                .orElseThrow(() -> new ResourceNotFoundException("User", "Id", id2));

        Searcher search = new Searcher();

        List<User> us = search.find(user,friend);

        return us;
    }

}