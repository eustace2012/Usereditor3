package com.example.demo.service;


import com.example.demo.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Класс сущность
 * */
public class Searcher {

    private List<User> used = new ArrayList();

    public List<User> find (User u1, User u2){

        List<User> result = new ArrayList();
        Set<User> friends = u1.getFriends();
        result.add(u1);
        used.add(u1);

        for(User user:  friends){
            if(used.contains(user)) {
                continue;
            }
            if(user.equals(u2)){
                result.add(u2);
                System.out.println("ins " + u2.getId());
                return result;
            } else {
                result.addAll(find(user,u2));
                if(result.contains(u2)){
                    return result;
                }
            }

        }
        result.remove(u1);
        return result;

    }
}
