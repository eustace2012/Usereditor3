package com.example.demo.web;


import com.example.demo.exception.InvalidDataException;
import com.example.demo.model.User;
import com.example.demo.service.UserServiceImpl;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



/** Класс контроллер Rest
 */
@RequestMapping("/api")
@RestController
public class UserController {

        private static final org.slf4j.Logger log = LoggerFactory.getLogger(UserController.class);

        @Autowired
        UserServiceImpl userService;


        /** Список всех пользователей */
        @GetMapping("/users")

        public List<User> getAllUsers() {
            return userService.getAll();
        }

        /** Добавить пользователя */
        @PostMapping("/users")
        public User createUser(@Valid @RequestBody User user) throws InvalidDataException {

            return userService.addUser(user);
        }

        /** Один пользователь */
        @GetMapping("/users/{id}")
        public User getUserById(@PathVariable(value = "id") Long userId) {

            return userService.getById(userId);
        }

        /** Редактировать пользователя*/
        @PutMapping("/users/{id}")
        public User updateUser(@PathVariable(value = "id") Long userId, @Valid @RequestBody User userDetails) throws InvalidDataException {

            User updatedUser = userService.updateUser(userDetails,userId);
            return updatedUser;
        }

        /** Удалить пользователя*/
        @DeleteMapping("/users/{id}")
        public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long userId) {


            userService.delete(userId);
            return ResponseEntity.ok().build();
        }

        /** Один пользователь по имени */
        @GetMapping("/username/{name}")
        public List<User> getUserByName(@PathVariable(value = "name") String name) {

            List<User> result = userService.getByName(name);
            return result;
        }

        /** Один пользователь по фамилии */
        @GetMapping("/usersurname/{surname}")
        public List<User> getUserBySurname(@PathVariable(value = "id") String surname) {

            List<User> result = userService.getBySurname(surname);
            return result;

        }

        /** Пользователи родившиеся позже указанной даты */
        @GetMapping("/later/{date}")
        public List<User> getUserLater(@PathVariable(value = "date") String date) throws InvalidDataException {

            Date d = null;
            try {
                d = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            } catch (Exception ex) {
                throw new InvalidDataException("дата  должна быть в формате yyyy-MM-dd");
            }

            List<User> users =  userService.getAfter(date);

            return users;

        }

        /** Пользователи родившиеся раньше указанной даты */
        @GetMapping("/earlier/{date}")
        public List<User> getUserEarlier(@PathVariable(value = "date") String date) throws InvalidDataException {

            Date d = null;
            try {
                d = new SimpleDateFormat("yyyy-MM-dd").parse(date);
            } catch (Exception ex) {
                throw new InvalidDataException("дата  должна быть в формате yyyy-MM-dd");
            }

            List<User> users =  userService.getBefore(date);
            return users;

        }



        /** Добавить друга */
        @PutMapping("/useraddfriend/{id}")
        public User addFriend(@PathVariable(value = "id") Long userId, @Valid @RequestBody User userDetails) throws InvalidDataException {

            User result = userService.addFriend(userId,userDetails.getId());
            return result;
        }

        /** Удалить друга */
        @PutMapping("/userdeletefriend/{id}")
        public User deleteFriend(@PathVariable(value = "id") Long userId, @Valid @RequestBody User userDetails) throws InvalidDataException {

            User result = userService.deleteFriend(userId,userDetails.getId());
            return result;
        }

        /** Цепочка друзей */
        @PostMapping("/friendschain/{id}")
        public List<User> getFriendsChain(@PathVariable(value = "id") Long userId, @Valid @RequestBody User userDetails) throws InvalidDataException {

            List<User> result = userService.getChain(userId,userDetails.getId());
            return result;
        }


    }
