package com.example.demo.web;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.example.demo.DemoApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DemoApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

    }



    /**
     * Проверяем количество возвращенных пользователей
     */
    @Test
    public void checkNumber() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users"))
                .andExpect(jsonPath("$", hasSize(6))).andDo(print());
    }

    /**
     * Получаем пользователя по id
     */
    @Test
    public void verifyUserById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.id").value(2))
                .andExpect(jsonPath("$.name").value("TEST2_NAME"))
                .andExpect(jsonPath("$.secondName").value("TEST_2_SECOND_NAME"))
                .andExpect(jsonPath("$.surname").value("TEST_2_SURNAME"))
                .andExpect(jsonPath("$.birthday").value("2005-08-12"))
                .andDo(print());
    }



    /**
     * Проверяем удаление пользователя.
     */
    @Test
    public void verifyDeleteUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/users/6").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print());
    }


    /**
     * Проверяем ошибку при удалении несуществующего пользователя.
     */
    @Test
    public void verifyInvalidUserIdToDelete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/users/9").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(404))
                .andDo(print());
    }



    /**
     * Проверяем сохранение пользователя.
     */
    @Test
    public void verifySaveUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\" : \"TEST_SAVE\", \"secondName\" : \"TEST_SAVE\", \"surname\" : \"TEST_SAVE\", \"birthday\" : \"2018-05-08\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name").value("TEST_SAVE"))
                .andExpect(jsonPath("$.secondName").value("TEST_SAVE"))
                .andExpect(jsonPath("$.surname").value("TEST_SAVE"))
                .andExpect(jsonPath("$.birthday").value("2018-05-08"))
                .andDo(print());
    }


    /**
     * Проверяем изменение полей пользователя.
     */
    @Test
    public void verifyUpdateUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/api/users/3")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"name\": \"TEST3_UPDATED\", \"secondName\" : \"TEST3_UPDATED\", \"surname\" : \"TEST3_UPDATED\", \"birthday\" : \"2007-05-08\" }")
                .accept(MediaType.APPLICATION_JSON))
                //.andExpect(jsonPath("$.id").exists())
                //.andExpect(jsonPath("$.id").value(3))
                //.andExpect(jsonPath("$.name").value("TEST3_UPDATED"))
                //.andExpect(jsonPath("$.secondName").value("TEST3_UPDATED"))
                //.andExpect(jsonPath("$.surname").value("TEST3_UPDATED"))
                //.andExpect(jsonPath("$.birthday").value("2007-05-08"))
                .andDo(print());
    }


    /**
     * Проверяем  список пользователей, родившихся после определенной даты.
     */
    @Test
    public void verifyGetBirthdayAfter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/later/2007-12-12"))
                .andExpect(jsonPath("$", hasSize(2))).andDo(print());

    }

    /**
     * Проверяем  список пользователей, родившихся ранее определенной даты.
     */
    @Test
    public void verifyGetBirthdayBefore() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/earlier/2007-01-01"))
                .andExpect(jsonPath("$", hasSize(3))).andDo(print());

    }


    /**
     * Проверяем добавление друга пользователю.
     */
    @Test
    public void verifyAddFriend() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/api/useraddfriend/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":3, \"name\": \"TEST3_UPDATED\", \"secondName\" : \"TEST3_UPDATED\", \"surname\" : \"TEST3_UPDATED\", \"birthday\" : \"2007-05-08\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.id").value(1))
                .andDo(print());
    }


    /**
     * Проверяем цепочку друзей.
     */
    @Test
    public void verifyChainFriend() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/friendschain/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":2, \"name\": \"TEST\", \"secondName\" : \"TEST\", \"surname\" : \"TEST\", \"birthday\" : \"2007-05-08\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[1].id", is(5)))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[3].id", is(4)))
                .andExpect(jsonPath("$[4].id", is(2)))
                .andDo(print());
    }


    /**
     * Проверяем  удаление из друзей.
     */
    @Test
    public void verifyDeleteFriend() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/api/userdeletefriend/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"id\":3, \"name\": \"TEST3_UPDATED\", \"secondName\" : \"TEST3_UPDATED\", \"surname\" : \"TEST3_UPDATED\", \"birthday\" : \"2007-05-08\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.id").value(1))
                .andDo(print());
    }


}